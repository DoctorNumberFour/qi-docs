window.onload = function(){
  $('.sidenav').scrollTop(Cookies.get('scrollPos'));
  var hash = location.hash.substring(1);
  if (hash.length > 0) {
  	window.location.href='#'+hash;
  }
}

$('.hamburger').click(function(){
  $('.sidenav').toggle();
});

 // Cache selectors
 var topMenu = $("#tree-container"),
     topMenuHeight = topMenu.outerHeight()+15,
     // All list items
     menuItems = topMenu.find("#link-"+window.location.pathname.slice(window.location.pathname.lastIndexOf('/') + 1)+" a.section-link");

 // Bind to scroll
 $('.outside').scroll(function(){
    // Get container scroll position
    var distance = 0;
    var id = "";
    $('.outside .section-header, .outside .video-fig').each(function(i){
      var scrollTop = $('.main').scrollTop();
      var elementOffset = $(this).offset().top;
      var curDis = (elementOffset - scrollTop);
      if(curDis <= 150){
          distance      = curDis;
          id = $(this).attr("id");
        }
    });

    console.log('active: ' + id);
    // Set/remove active class

    menuItems.parent().removeClass("active");
    menuItems.filter("[href*='#"+ (id.localeCompare("") == 0 ? "/" : id) +"']").parent().addClass("active");
 });
 $('.sidenav').scroll(function(){
   Cookies.set('scrollPos', '' + $('.sidenav').scrollTop());
 });

 $("select.chaps").change(function(){
      var time = $(this).children("option:selected").val().split(":");
      var timeSecs = time.length == 3 ? (parseInt(time[0]) * 3600) + (parseInt(time[1]) * 60) + parseInt(time[2]) : (parseInt(time[0]) * 60) + parseInt(time[1]);
      console.log("clicked to jump to " + time.join(":") + " (" + timeSecs + ")");
      $(this).siblings("video").get(0).currentTime = timeSecs;
      $(this).val("nothin");
   });
   $('.indent-link').click(function() {
           $(this).toggleClass('rotated');
       });
