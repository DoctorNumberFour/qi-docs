<?php snippet('search-header') ?>
<form>
  <input type="search" name="q" value="<?= html($query) ?>">
  <input type="submit" value="Search">
</form>

<ul>
  <?php foreach ($results as $result): ?>
  <li class="search-result">
    <a class="search-result-name" href="<?= $result->url() ?>">
      <?= $result->title() ?>
    </a>
    <a class="search-result-preview" href="<?= $result->url() ?>">
      <?= $result->preview() ?>
    </a>
  </li>
  <?php endforeach ?>
</ul>
<?php snippet('doc-footer') ?>
