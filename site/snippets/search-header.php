<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $site->title() ?></title>
  <?= css('assets/css/index.css') ?>
  <?= js([
  'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
  'assets/js/app.js'
]) ?>
</head>

<body>
  <header>

    <nav class="menu">
    <?php foreach($site->breadcrumb() as $crumb): ?>
    <div>
      <a href="<?= $crumb->url() ?>" <?= e($crumb->isActive(), 'aria-current="page"') ?>>
        <?= html($crumb->title()) ?>
      </a>
    </div>
    <?php endforeach ?>


    </nav>


  </header>
  <div class="header-outside">
  <div class="top">
    <div class='hamburger'><?= $site->image('hamburger.png') ?></div>
    <h1 class="header">
      <?= $page->title() ?>
    </h1>
    <div class="logo"><?= $site->image('logo.png')->resize(100) ?></div>
    </div>
  <div class="outside">
    <div class="sidenav">
      <div class="tree-container">
        <?php snippet('treemenu') ?>
      </div>
    </div>
  <div class="main">