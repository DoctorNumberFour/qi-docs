<figure class="video-fig <?= $block->id() ?>" id="<?= Str::slug($block->text()) ?>">
  <?php if($video = $block->video()->toFile()): ?>
    <video width="100%" controls>
      <source src="<?= $video->url() ?>"/>
    </video>
  <?php endif ?>
  <?php if($block->hasChapters()->toBool() and (! $block->chapters()->toStructure()->isEmpty())): ?>
    <select class="form-select chaps" aria-label="chapters">
      <option disabled selected value="nothin">Jump to chapter...</option>
      <?php foreach ($block->chapters()->toStructure() as $chap) : ?>
        <option value=<?= $chap->timestamp() ?> ><?= $chap->chapter() ?></option>
      <?php endforeach ?>
    </select>
    <!-- <button class="jmp">Jump</button> -->
  <?php endif ?>
  <figcaption>
    <?= $block->caption() ?>
  </figcaption>
</figure>
