<figure class="icon"/>
<?php if($image = $block->image()->toFile()): ?>
  <img src="<?= $image->url() ?>" alt="">
<?php endif ?>
    </figure>
<<?= $level = $block->level()->or('h2') ?> class="doc-title"><?= $block->text() ?></<?= $level ?>>
