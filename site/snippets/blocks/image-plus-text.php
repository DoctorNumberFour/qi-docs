<div class="row <?php if(strcmp($block->layout(), "left") == 0): ?> flex-row-reverse <?php endif ?>">
  <div class="col-md-6">
    <p class="column-text"><?= $block->text()?></p>
  </div>
  <div class="col-md-6 <?= $block->resize() ? "image-size-" . $block->size() : "" ?>">
    <?php if($image = $block->image()->toFile()): ?>
      <img class="column-image shadow-none" src="<?= $image->url() ?>" alt="">
    <?php endif ?>
  </div>
</div>
