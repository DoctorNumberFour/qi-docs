</div>
</div>

  <?= js([
    'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
    'assets/js/js.cookie.js',
    "vendor/twbs/bootstrap/dist/js/bootstrap.js",
    'assets/js/app.js'
  ]) ?>
  <script type="text/javascript">
  function storeStateAjax(id) {
        $.ajax({
             method: "POST",
             url: '../assets/php/storeSession.php',
             data:{action:'store', id:id, class:$("#" + id).attr('class').localeCompare("collapse") != 0 ? "collapse show" : "collapse"}

        });
   }
   function storeStateAjaxOnClick(id) {
         $.ajax({
              method: "POST",
              url: '../assets/php/storeSession.php',
              data:{action:'store', id:id, class:$("#" + id).attr('class').localeCompare("collapse") == 0 ? "collapse show" : "collapse"}

         });
    }
  $(function(){
    <?php foreach($site->page()->parents() as $p): ?>
    storeStateAjax("<?= "link-" . $p->slug() ?>");
    <?php endforeach ?>
    storeStateAjax("<?= "link-" . $site->page()->slug() ?>");
  });
  </script>
</body>
</html>
