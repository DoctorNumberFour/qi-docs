<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $site->title() ?></title>
  <?= css([
    "vendor/twbs/bootstrap/dist/css/bootstrap.css",
    'assets/css/index.css'
    ]) ?>
</head>

<body>
  <header>
    <div class="logo"><?= $site->image('logo.png') ?></div>
    <nav class="menu">
      <?php foreach ($site->breadcrumb() as $crumb) : ?>
        <div>
          <a href="<?= $crumb->url() ?>" <?= e($crumb->isActive(), 'aria-current="page"') ?>>
            <?= html($crumb->title()) ?>
          </a>
        </div>
      <?php endforeach ?>


    </nav>


  </header>

        <div class='hamburger'><?= $site->image('hamburger.png') ?></div>
    <div class="outside" id="outside">
      <div class="top">
        <div class="header">
          <h3><?= $page->title() ?></h3>
          <?php snippet("search") ?>
        </div>
      </div>
      <div class="sidenav">
        <div class="tree-container" id="tree-container">
          <?php snippet('treemenu') ?>
        </div>
      </div>
      <div class="main" id="main">
