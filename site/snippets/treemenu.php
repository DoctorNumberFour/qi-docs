<?php if (!isset($subpages)) $subpages = $site->children() ?>

  <?php foreach ($subpages->listed() as $p) : ?>
      <div class="indent depth-<?= $p->depth() ?>">
        <?php if ($p->text()->toBlocks()->filterBy('type', 'in', ['section-header', 'video-file'])->count() != 0 || $p->hasListedChildren()) : ?>
          <wrapper class="arrow-wrapper">
          <a href="#<?= "link-" . $p->slug() ?>" onclick="storeStateAjaxOnClick('<?= "link-" . $p->slug() ?>')" data-toggle="collapse" class="arrow indent-link <?= strpos($site->page()->parents()->append($site->page())->has($p->id()) ? "collapse show" : Cookie::get("link-" . $p->slug(), "collapse"), "show") !== false ? "rotated" : "" ?>">&#9650;</a>
        </wrapper>
        <?php else : ?>
          <span class='collapseless'/>
        <?php endif ?>
        <a class="<?= !($p->text()->toBlocks()->filterBy('type', 'in', ['section-header', 'video-file'])->count() != 0 || $p->hasListedChildren()) ? "indent-link " : "" ?><?= $p->isActive() ? "active" : "" ?>" href="<?= $p->url() ?>"><?= $p->title()->html() ?></a>
        <div id="<?= "link-" . $p->slug() ?>" class="<?= $site->page()->parents()->append($site->page())->has($p->id()) ? "collapse show" : Cookie::get("link-" . $p->slug(), "collapse") ?>" >
        <?php if ($p->text()->blocks() != null) : ?>
            <?php foreach ($p->text()->toBlocks()->filterBy('type', 'in', ['section-header', 'video-file'])->getIterator() as $block) : ?>
              <?php if ($block != null and $block->text()) : ?>
                  <div class="section-indent depth-<?= $p->depth() ?>">
                    <a class="section-link indent-link" href="<?= $p->url() ?>#<?= Str::slug($block->content()->get("text")) ?>">↳  <?= $block->content()->get("text") ?></a>
                  </div>
              <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>
        <?php if ($p->hasChildren()) : ?>
          <?php snippet('treemenu', ['subpages' => $p->children()]) ?>
        <?php endif ?>
    </div>
      </div>
  <?php endforeach ?>
