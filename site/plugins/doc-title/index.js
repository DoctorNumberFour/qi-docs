panel.plugin("qi-docs/doc-title-block", {
  blocks: {
    "doc-title":
    {
      render: function (createElement){
        return createElement(
      this.content.level,   // tag name
      {
        on: {
          click: this.open
        },
        style: {
          color: (this.content.text ? 'black' : '#999999')
        }
      },
      [this.content.text ? this.content.text : "Title..."] // array of children
    );
      }
  }
  }
});
