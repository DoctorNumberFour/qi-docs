panel.plugin("qi-docs/image-plus-text-block", {
  blocks: {
    "image-plus-text":
    {
      methods: {
        plcmnt: function(layout){
          return "flex-direction: " + ("left".localeCompare(layout) == 0 ? "row-reverse" : "row");
        }
      },
      computed: {
        imagePresent(){
          return this.content.image.length != 0;
        },
        size(){
          if(this.content.resize){
            return this.content.size;
          } else {
            return "";
          }
        }
      },
      template: `
      <div class="row" :style="plcmnt(content.layout)">
        <div class="column text-column">
          <div v-html="content.text"/>
        </div>
        <div class="column image-column" @click="open">
          <img :class="size" v-if="imagePresent" :src="content.image[0].url">
        </div>
      </div>
    `
  }
  }
});
