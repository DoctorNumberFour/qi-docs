panel.plugin("qi-docs/resizable-image-block", {
  blocks: {
    "resizable-image":
    {
      computed: {
        imagePresent(){
          return this.content.image.length != 0;
        },
        size(){
          return this.content.size;
        }
      },
      template: `
      <div @click="open">
        <img :class="size" v-if="imagePresent" :src="content.image[0].url"/>
      </div>
    `
  }
  }
});
