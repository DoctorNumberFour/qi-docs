panel.plugin("qi-docs/video-file-block", {
  blocks: {
    "video-file":
    {
      computed: {
        videoPresent(){
          return this.content.video.length != 0;
        }
      },
      template: `
      <div @click="open">
        <video v-if="videoPresent" width="100%" controls>
          <source :src="content.video[0].url"/>
        </video>
        <div v-html="content.caption"/>
      </div>
    `
  }
  }
});
