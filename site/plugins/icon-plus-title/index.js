panel.plugin("qi-docs/icon-plus-title-block", {
  blocks: {
    "icon-plus-title":
    {
      computed: {
        imagePresent(){
          return this.content.image.length != 0;
        }
      },
      template: `
      <div>
        <div class="icon" @click="open">
          <img v-if="imagePresent" :src="content.image[0].url">
        </div>
        <h2 v-html="content.text" class="text"/>
      </div>
    `
  }
  }
});
