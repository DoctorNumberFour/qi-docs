panel.plugin("qi-docs/icon-block", {
  blocks: {
    icon:
    {
      computed: {
        imagePresent(){
          return this.content.image.length != 0;
        }
      },
      template: `
      <div class="icon" @click="open">
        <img v-if="imagePresent" :src="content.image[0].url"/>
      </div>
    `
  }
  }
});
