<?php
return [
  'thathoff' => [
    'git-content' => [
      'commit' => true,
      // 'push' => true,
      'pull' => true,
      'displayErrors' => true,
      'gitBin' => 'git',
      'disable' => true
    ]
  ],
  'panel' =>[
    'install' => true
  ],
  'debug'  => true
];
